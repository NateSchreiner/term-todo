package main

import (
	"fmt"
	"github.com/alexeyco/simpletable"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"strings"
)

func quicksort(arr []int, low, high int) {
	if low < high {
		pivot := partition(arr, low, high)
		quicksort(arr, low, pivot-1)
		quicksort(arr, pivot+1, high)
	}

	fmt.Println(arr)
}

func partition(arr []int, low, high int) int {
	p := arr[high]
	i := low - 1

	for j := low; j < high; j++ {
		if arr[j] <= p {
			i++
			arr[i], arr[j] = arr[j], arr[i]
		}
	}

	arr[i+1], arr[high] = arr[high], arr[i+1]
	return i + 1
}

func main() {
	// selectableTableDemo()
	// return
	//app := tview.NewApplication()
	a := []int{9, 234, 29, 1, 0, 45, 92, 6, 2}

	quicksort(a, 0, len(a)-1)
	fmt.Println(a)

	_ = newTasks()
	//_, err := WriteTasks(tasks)
	//t, err := ReadTasks()
	//if err != nil {
	//	panic(err)
	//}
	//
	//fmt.Println(t)

	//initSimpleTable(tasks)
	//multiLineDemo()
	//table := initTable(tasks, app)

	//if err := app.SetRoot(table, true).SetFocus(table).Run(); err != nil {
	//	panic(err)
	//}
}

func initTable(tasks []Task, app *tview.Application) *tview.Table {

	table := tview.NewTable().
		SetBorders(true)

	//cols, row := 3, len(tasks)

	for idx, task := range tasks {
		setCells(table, idx, task)
	}

	table.Select(0, 0).SetFixed(1, 1).SetDoneFunc(func(key tcell.Key) {
		if key == tcell.KeyEscape {
			app.Stop()
		}
		if key == tcell.KeyEnter {
			table.SetSelectable(true, true)
		}
	}).SetSelectedFunc(func(row int, column int) {
		table.GetCell(row, column).SetTextColor(tcell.ColorRed)
		table.SetSelectable(false, false)
	})

	return table
}

func initSimpleTable(tasks []Task) {
	table := simpletable.New()

	cells := make([][]*simpletable.Cell, 5)
	for idx := range cells {
		cells[idx] = make([]*simpletable.Cell, 5)
	}

	for idx, task := range tasks {
		cell := &simpletable.Cell{
			Align: simpletable.AlignCenter,
			Text:  fmt.Sprintf("%d", task.Id),
		}
		cells[idx][0] = cell

		//cells[idx] = append(cells[idx], cell)

		cell = &simpletable.Cell{
			Align: simpletable.AlignCenter,
			Text:  task.Task,
		}
		cells[idx][1] = cell
		//cells[idx] = append(cells[idx], cell)

		cell = &simpletable.Cell{
			Align: simpletable.AlignCenter,
			Text:  task.Done,
		}
		cells[idx][2] = cell
		//cells[idx] = append(cells[idx], cell)

		cell = &simpletable.Cell{
			Align: simpletable.AlignCenter,
			Span:  2,
			Text:  task.Note,
		}
		cells[idx][3] = cell
		//cells[idx] = append(cells[idx], cell)

	}

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "ID"},
			{Align: simpletable.AlignCenter, Text: "Name"},
			{Align: simpletable.AlignCenter, Text: "Done"},
			{Align: simpletable.AlignCenter, Text: "Note"},
		},
	}

	table.Body = &simpletable.Body{
		Cells: cells,
	}

	table.Println()
}

var (
	leftAligned = `Multiline content
left aligned`

	centerAligned = `Multiline content
center aligned`

	rightAligned = `Multiline content
right aligned`

	singleLine = "Single line"
)

func multiLineDemo() {
	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "FOO"},
			{Align: simpletable.AlignCenter, Text: "BAR"},
			{Align: simpletable.AlignCenter, Text: "BAZ"},
		},
	}

	table.Body = &simpletable.Body{
		Cells: [][]*simpletable.Cell{
			{
				&simpletable.Cell{Align: simpletable.AlignLeft, Span: 2, Text: leftAligned},
				&simpletable.Cell{Text: singleLine},
			},
			{
				&simpletable.Cell{Align: simpletable.AlignCenter, Span: 3, Text: centerAligned},
			},
			{
				&simpletable.Cell{Text: singleLine},
				&simpletable.Cell{Align: simpletable.AlignRight, Span: 2, Text: rightAligned},
			},
		},
	}

	table.Println()
}

func setCells(table *tview.Table, idx int, task Task) {
	backgroundColor := tcell.ColorRed
	if idx%2 == 0 {
		backgroundColor = tcell.ColorBlack
	}
	table.SetCell(idx, 0, tview.NewTableCell(fmt.Sprintf("%d", task.Id)).
		SetBackgroundColor(backgroundColor).
		SetTextColor(tcell.ColorRed).
		SetAlign(tview.AlignCenter))

	table.SetCell(idx, 1, tview.NewTableCell(task.Task).
		SetBackgroundColor(backgroundColor).
		SetTextColor(tcell.ColorWhite).
		SetAlign(tview.AlignCenter))

	table.SetCell(idx, 2, tview.NewTableCell(fmt.Sprintf("%q", task.Done)).
		SetBackgroundColor(backgroundColor).
		SetTextColor(tcell.ColorWhite).
		SetAlign(tview.AlignCenter))

	table.SetCell(idx, 2, tview.NewTableCell(fmt.Sprintf("%q", task.Note)).
		SetBackgroundColor(backgroundColor).
		SetTextColor(tcell.ColorWhite).
		SetAlign(tview.AlignCenter))
}

func selectableTableDemo() {
	app := tview.NewApplication()
	table := tview.NewTable().
		SetBorders(true)
	lorem := strings.Split("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.", " ")
	cols, rows := 10, 40
	word := 0
	for r := 0; r < rows; r++ {
		for c := 0; c < cols; c++ {
			color := tcell.ColorWhite
			if c < 1 || r < 1 {
				color = tcell.ColorYellow
			}
			table.SetCell(r, c,
				tview.NewTableCell(lorem[word]).
					SetTextColor(color).
					SetAlign(tview.AlignCenter))
			word = (word + 1) % len(lorem)
		}
	}
	table.Select(0, 0).SetFixed(1, 1).SetDoneFunc(func(key tcell.Key) {
		if key == tcell.KeyEscape {
			app.Stop()
		}
		if key == tcell.KeyEnter {
			table.SetSelectable(true, true)
		}
	}).SetSelectedFunc(func(row int, column int) {
		table.GetCell(row, column).SetTextColor(tcell.ColorRed)
		table.SetSelectable(false, false)
	})
	if err := app.SetRoot(table, true).SetFocus(table).Run(); err != nil {
		panic(err)
	}

}
