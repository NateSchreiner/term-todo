package main

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
)

// Storage is used to store tasks and the todo into whatever mechanism is required.
// At this time, we utilize file storage on disk. We seperate each task by the bytes '\u0002'
// which according to ASCII is the <Start of Text> token.  Thus we can read tasks at particular
// indexes by just knowing the index of the corresponding '\u0002' in the file.

// This makes me think that at the beginning of this file, we should include a table with the
// indexes of all '\u0002' byte sequences

// The first byte `uint8` will be the number of `\u0002` included in the file (N)
// The second part will be a list of 4 byte values (uint32) which corresponds to the index.
// The first number here will tell us how big the header is

// Length:  1byte | 4b(each index)

var STX = '\u0002' // ASCII value 2.  (Start of Text)

func WriteTasks(tasks []Task) (int, error) {
	total := 0
	b := marshal(tasks)
	header := createHeader(b)
	n, err := writeHeader(header)
	if err != nil {
		return n, errors.New(fmt.Sprintf("Error writing header : %q", err))
	}

	total += n

	fmt.Printf("Wrote %d bytes for header\n", n)

	n, err = writeData(b, n)
	if err != nil {
		return n, errors.New(fmt.Sprintf("Error writing data to file : %q", err))
	}

	total += n
	return total, nil
}

func ReadTasks() ([]Task, error) {
	f, err := os.Open("./storage.json")
	if err != nil {
		return nil, err
	}

	bytes, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}

	header := getHeader(bytes)
	indexes := getIndexes(header)

	r := make([]Task, 0)

	// STX is 4 bytes wide so we need to account for that
	for i := 0; i < int(header[0])-1; i++ {
		begin := indexes[i] + 4
		end := indexes[i+1]
		task := bytes[begin:end]
		t := &Task{}
		err := json.Unmarshal(task, &t)
		if err != nil {
			return nil, err
		}

		r = append(r, *t)
	}

	return r, nil
}

// 1. Create function that takes list of tasks and returns each tasks length (how many bytes to write)
// used to determine the indexes of all STX
func marshal(tasks []Task) [][]byte {
	r := make([][]byte, len(tasks))

	for idx, task := range tasks {
		v, err := json.Marshal(task)
		if err != nil {
			panic(err)
		}

		r[idx] = v
	}

	return r
}

// STX Indexes have to be bigger than 1 byte (Max value 255).  Need to upgrade the indexes to be 4 byte values (uint32)
func createHeader(arr [][]byte) []byte {
	r := make([]byte, 1)
	r[0] = byte(len(arr) + 1)

	// the first STX byte written to file will be 1 + the number of STX indexes
	lastStxLoc := uint32(1 + (len(arr)+1)*4)

	tmp := make([]byte, 4)
	binary.LittleEndian.PutUint32(tmp, lastStxLoc)
	r = append(r, tmp...)
	for _, task := range arr {
		l := len(task) + 3
		lastStxLoc += uint32(l + 1)
		binary.LittleEndian.PutUint32(tmp, lastStxLoc)
		r = append(r, tmp...)
	}

	return r
}

func getHeader(fileData []byte) []byte {
	first := fileData[0]
	bufSize := (uint8(first) * 4) + 1
	header := fileData[0:bufSize]
	return header
}

func writeHeader(header []byte) (int, error) {
	f, err := os.OpenFile("./storage.json", os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return 0, err
	}

	defer f.Close()

	n, err := f.Write(header)
	return n, err
}

func writeData(bytes [][]byte, headerSize int) (int, error) {
	f, err := os.OpenFile("./storage.json", os.O_WRONLY, 0666)
	if err != nil {
		panic(err)
	}

	total := 0

	buf := make([]byte, 4)
	binary.LittleEndian.PutUint32(buf, uint32(STX))
	f.Seek(int64(headerSize), 0)
	for idx, task := range bytes {
		n, err := f.Write(buf)
		if err != nil {
			return n, errors.New(fmt.Sprintf("Could not write STX before task #%d : %q", idx, err))
		}
		total += n
		n, err = f.Write(task)
		if err != nil {
			return n, errors.New(fmt.Sprintf("Could not write task #%d : %q", idx, err))
		}
		total += n
	}

	return total, nil
}

func getIndexes(header []byte) []int {

	r := make([]int, 0)
	// 3 tasks, 4 indexes takes up 17 bytes total
	for i := 1; i < len(header); i += 4 {
		index := binary.LittleEndian.Uint32(header[i:])
		fmt.Println(index)
		r = append(r, int(index))
	}

	return r
}

func calculateHeaderSize(n int) int {
	return ((n + 1) * 4) + 1
}
