package main

import (
	"encoding/binary"
	"fmt"
	"testing"
)

func Test_createHeader(t *testing.T) {
	tasks := newTasks()
	bytes := marshal(tasks)
	buf := createHeader(bytes)
	// check buf[0] == 3
	if buf[0] != uint8(len(tasks)+1) {
		t.Fatalf("buf[0] value not expected. Looking for: %d. Got=%d", uint8(len(tasks)+1), buf[0])
	}

	firstStx := 1 + (len(tasks)+1)*4
	num := binary.LittleEndian.Uint32(buf[1:5])
	if num != uint32(firstStx) {
		t.Fatalf("buf[1:5] not expected.  Expecting: %d.  Got=%d", num, firstStx)
	}

	secondToLast := binary.LittleEndian.Uint32(buf[9:])
	fmt.Println("SecondToLast: ", secondToLast)
	fmt.Println("Size of last task: ", len(bytes[2]))
	lastStxIndex := binary.LittleEndian.Uint32(buf[13:])
	fmt.Println(lastStxIndex)
	fmt.Println("stop")
}
