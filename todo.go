package main

import "time"

type Todo struct {
	Tasks []Task
}

type Task struct {
	Id       int
	Task     string
	Done     string
	Note     string
	Created  time.Time
	Finished time.Time
}

func newTasks() []Task {
	r := []Task{
		{
			Id:   1,
			Task: "First Task		",
			Done: "No",
			Note: "LLorem ipsum itsull satsatl forst asldLorem ipsum itsull satsatl forst asldorem ipsum itsull satsatl forst asld",
		},
		{
			Id:   2,
			Task: "SEcond Task		",
			Done: "Yes",
			Note: "LLorem ipsum itsull satsatl forst asldLorem ipsum itsull satsatl forst asldorem ipsum itsull satsatl forst asld",
		},
		{
			Id:   3,
			Task: "Third Task		",
			Done: "No",
			Note: "LLorem ipsum itsull satsatl forst asldLorem ipsum itsull satsatl forst asldorem ipsum itsull satsatl forst asld",
		},
	}

	return r

}
